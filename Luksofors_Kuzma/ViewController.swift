//
//  ViewController.swift
//  Luksofors_Kuzma
//
//  Created by Students on 28/03/2021.
//

import UIKit
import SwiftUI


class ViewController: UIViewController {
    @IBOutlet var timeSelector: UIDatePicker!
    @IBOutlet var drawView: DrawView!
    @IBOutlet var nextSignal: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ggg")
        print(timeSelector.date.timeIntervalSince1970)
        print("eee")
    }
    
    @IBAction func timerUpdate(_ sender: Any) {
        let time = timeSelector.date.timeIntervalSince1970-10800
        drawView.changeColor(time: time)
        nextSignal.text?.removeAll()
        if Int(time) % 420 < 60 {
            nextSignal.text?.append("Yellow signal in 2 min.")
        } else if Int(time) % 420 < 120 {
            nextSignal.text?.append("Yellow signal in 1 min.")
        } else if Int(time) % 420 < 180 {
            nextSignal.text?.append("Red signal in 1 min.")
        } else if Int(time) % 420 < 240 {
            nextSignal.text?.append("Yellow signal in 3 min.")
        } else if Int(time) % 420 < 300 {
            nextSignal.text?.append("Yellow signal in 2 min.")
        } else if Int(time) % 420 < 360 {
            nextSignal.text?.append("Yellow signal in 1 min.")
        } else {
            nextSignal.text?.append("Green signal in 1 min.")
        }
        
    }
    
    
}


