//
//  DrawView.swift
//  Luksofors_Kuzma
//
//  Created by Students on 29/03/2021.
//

import UIKit

class DrawView: UIView {
    var color: CGColor = UIColor.green.cgColor
    
    override func draw(_ rect: CGRect) {
        guard let currentContext = UIGraphicsGetCurrentContext() else {
            return
        }
        drawCircle(user: currentContext, color: color)
    }
    
    private func drawCircle(user context: CGContext, color: CGColor) {
        let rad: CGFloat = bounds.size.width/2-30
        let centerPoint = CGPoint(x: bounds.size.width/2, y: bounds.size.height/2-20)
        context.setLineCap(.square)
        context.setLineWidth(8.0)
        context.addArc(center: centerPoint, radius: rad, startAngle: CGFloat(0), endAngle: CGFloat(0.001), clockwise: true)
        context.setStrokeColor(UIColor.black.cgColor)
        context.strokePath()
        context.addArc(center: centerPoint, radius: rad, startAngle: CGFloat(0), endAngle: CGFloat(0.001), clockwise: true)
        context.setFillColor(color)
        context.fillPath()
    }
    
    func changeColor(time: Double) {
        setNeedsDisplay()
        if Int(time) % 420 < 120 {
            color = UIColor.green.cgColor
        } else if Int(time) % 420 < 180 {
            color = UIColor.yellow.cgColor
        } else if Int(time) % 420 < 360 {
            color = UIColor.red.cgColor
        } else {
            color = UIColor.yellow.cgColor
        }
    }

}
